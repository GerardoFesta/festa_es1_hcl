import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;

public class Lexer {

    private RandomAccessFile input;
    private static HashMap<String, Token> stringTable;  // la struttura dati potrebbe essere una hash map
    private int state;


    public Lexer(){
        // la symbol table in questo caso la chiamiamo stringTable
        stringTable = new HashMap<String, Token>();
        state = 0;
        // if then else while int float
        stringTable.put("if", new Token("IF"));   // inserimento delle parole chiavi nella stringTable per evitare di scrivere un diagramma di transizione per ciascuna di esse (le parole chiavi verranno "catturate" dal diagramma di transizione e gestite e di conseguenza). IF poteva anche essere associato ad una costante numerica
        stringTable.put("then", new Token("THEN"));
        stringTable.put("else", new Token("ELSE"));
        stringTable.put("while", new Token("WHILE"));
        stringTable.put("int", new Token("INT"));
        stringTable.put("float", new Token("FLOAT"));

    }

    public Boolean initialize(String filePath){
        try {
            input= new RandomAccessFile(filePath, "r");
            input.seek(0);
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
        // prepara file input per lettura e controlla errori

    }

    public Token nextToken() throws Exception{

        //Ad ogni chiamata del lexer (nextToken())
        //si resettano tutte le variabili utilizzate
        state = 0;
        StringBuilder lessema = new StringBuilder(); // il lessema riconosciuto
        char c;
        //..
        try {
            while (true) {

                // legge un carattere da input e lancia eccezione quando incontra EOF per restituire null
                //  per indicare che non ci sono più token

                c = (char)input.read();


                if(!((Character.isSpaceChar(c) || Character.isWhitespace(c)) || Character.isLetter(c) || Character.isDigit(c) || (c == '<' || c == '>' || c == '=') || (c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}' || c == ';' || c == ',' || c == '.' || c == '+' || c == '-') || c == '\uFFFF'))
                    throw new LexerException("Unrecognized char");

                switch (state) {
                    case 0:
                        if (Character.isSpaceChar(c) || Character.isWhitespace(c)) {
                            state = 1;
                            //System.out.println("Leggo vuoto");
                        }
                        else if(Character.isLetter(c)) {
                            state = 4;

                        }else if(Character.isDigit(c)|| c == '.') {
                            state = 7;

                        }else if(c == '<' || c == '>' || c == '='){
                            state = 25;

                        }else if(c == '('){
                            state=17;

                        }else if(c == ')') {
                            state = 18;

                        }else if(c == '['){
                            state=19;

                        }else if(c == ']'){
                            state=20;

                        }else if(c == '{'){
                            state=21;

                        }else if(c == '}'){
                            state=22;

                        }else if(c == ';'){
                            state=23;

                        }else if(c == ','){
                            state=24;

                        }else if(c == '\uFFFF'){
                            return null;
                        }else if(c == '+' || c== '-') {
                            state = 36;
                        }else{
                            throw new LexerException("Unrecognized char");
                        }


                        break;
                }
                //sep
                switch (state) {

                    case 1:
                        if (Character.isSpaceChar(c) || Character.isWhitespace(c))
                            state = 2;
                        break;
                    case 2:
                        if (Character.isSpaceChar(c) || Character.isWhitespace(c))
                            state = 2;
                        else
                            state = 3;

                    case 3:
                        retrack();
                        if (c == '\uFFFF') return null;
                        state = 0;
                        break;


                }


                //id
                switch (state) {
                    case 4:
                        if (Character.isLetter(c)) {
                            state = 5;
                            lessema.append(c);

                        }
                        break;
                    case 5:
                        if (Character.isLetterOrDigit(c)) {
                            state=5;
                            lessema.append(c);

                            break;
                        }else if (c == '\uFFFF') // controlla se è finito il file
                            return installID(lessema.toString());
                        else {
                            state = 6;
                        }

                    case 6:

                        retrack();
                        return installID(lessema.toString());

                }//end switch


                //unsigned numbers
                switch (state) {
                    case 7:
                        if (Character.isDigit(c)) {
                            state = 8;
                            lessema.append(c);

                        } else if (c == '.') {
                            state = 9;
                            lessema.append(c);

                        }
                        break;
                    case 8:
                        if (Character.isDigit(c)) {
                            state = 8;
                            lessema.append(c);
                            break;
                        } else if (c == '.') {
                            state = 9;
                            lessema.append(c);

                            break;
                        } else if (c == 'E') {
                            lessema.append(c);
                            state = 13;

                            break;
                        }else if (c == '\uFFFF')
                            return new Token("NUMBER", lessema.toString());
                        else {
                            state = 10;
                            //System.out.println("SETTATO STATO A 10");
                            retrack();
                        }

                    case 9:
                        if (Character.isDigit(c)) {
                            lessema.append(c);
                            state = 11;

                        }else if (c == '\uFFFF')
                            return new Token("NUMBER", lessema.toString());
                        break;
                    case 10:
                        retrack();
                        return new Token("NUMBER", lessema.toString());

                    case 11:
                        if (Character.isDigit(c)) {
                            lessema.append(c);
                            state = 11;


                            break;
                        } else if (c == 'E') {
                            lessema.append(c);
                            state = 13;
                            break;
                        }else if (c == '\uFFFF')
                            return new Token("NUMBER", lessema.toString());
                        else
                            state = 12;


                    case 12:
                        retrack();
                        //System.out.println("PRIMA DI RETURN");
                        return new Token("NUMBER", lessema.toString());

                    case 13:
                        if (c == '+' || c == '-') {
                            lessema.append(c);
                            state = 14;
                            break;
                        } else if (Character.isDigit(c)) {
                            lessema.append(c);
                            state = 15;
                            break;
                        }else if (c == '\uFFFF')
                            return new Token("NUMBER", lessema.toString());
                        else{
                            retrack();
                            return new Token("NUMBER", lessema.toString());

                        }

                    case 14:
                        if (Character.isDigit(c)) {
                            lessema.append(c);
                            state = 15;
                            break;
                        }else if (c == '\uFFFF')
                            return new Token("NUMBER", lessema.toString());
                        else {
                            retrack();
                            return new Token("NUMBER", lessema.toString());
                        }

                    case 15:
                        if (Character.isDigit(c)) {
                            lessema.append(c);
                            state = 15;
                            break;

                        }else if (c == '\uFFFF')
                            return new Token("NUMBER", lessema.toString());
                        else
                            state = 16;

                    case 16:
                        retrack();
                        return new Token("NUMBER", lessema.toString());

                    default:
                        break;
                }

                //separatori
                switch (state) {
                    case 17:
                        if (c == '('){
                            //System.out.println("PARENTESI TONDA APERTA");
                            return new Token("OPRO");

                        }
                        break;
                    case 18:
                        if (c == ')') return new Token("CLRO");
                        break;
                    case 19:
                        if (c == '[') return new Token("OPSQ");
                        break;
                    case 20:
                        if (c == ']') return new Token("CLSQ");
                        break;
                    case 21:
                        if (c == '{') return new Token("OPGR");
                        break;
                    case 22:
                        if (c == '}') return new Token("CLGR");
                        break;
                    case 23:
                        if (c == ';') return new Token("SEMI");
                        break;
                    case 24:
                        if (c == ',') return new Token("COMM");
                        break;
                }

                //relop
                switch (state) {
                    case 25:
                        if (c == '>') {
                            //System.out.println("CASE 25 >");
                            state = 26;
                        }else if (c == '='){
                            //System.out.println("RICONOSCIUTO UGUALE");
                            state = 29;
                        }

                        else if (c == '<')
                            state = 30;
                        break;
                    case 26:
                        if (c == '='){
                            //System.out.println("CASE 26 =");
                            state = 27;
                        } else
                            state = 28;

                    case 27:
                        //System.out.println("CASE 27 RETURN");
                        return new Token("RELOP", "GE");

                    case 28:
                        retrack();
                        return new Token("RELOP", "GT");


                    case 29:
                        retrack();
                        return new Token("RELOP", "EQ");
                    case 30:
                        if (c == '>')
                            state = 31;
                        else if (c == '=')
                            state = 32;
                        else if (c == '-')
                            state = 33;
                        else state = 35;
                        break;
                    case 31:
                        return new Token("RELOP", "NE");
                    case 32:
                        return new Token("RELOP", "LE");
                    case 33:
                        if (c == '-')
                            state = 34;
                        else{
                            state = 35;
                            break;
                        }

                    case 34:
                        if (c == '-')
                            return new Token("ASSIGN");
                    case 35:
                        retrack();
                        retrack();
                        return new Token("RELOP", "LT");


                }

                switch(state){
                    case 36:
                        if(c == '+') return new Token("PLUS");
                        else if (c == '-') return new Token("MINU");
                }
            }
        }catch (IOException e){
            return null;
        }
    }
//end while
	//end method


    private Token installID(String lessema){
        Token token;

        //utilizzo come chiave della hashmap il lessema
        if(stringTable.containsKey(lessema))
            return stringTable.get(lessema);
        else{
            token =  new Token("ID", lessema);
            stringTable.put(lessema, token);
            return token;
        }
    }


    private void retrack(){
    // fa il retract nel file di un carattere
        try {
            long curr_off = input.getFilePointer();
            input.seek(curr_off-1);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

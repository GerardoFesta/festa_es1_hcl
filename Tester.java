import java.io.*;
import java.nio.charset.Charset;

public class Tester {

    public static void main(String[] args) throws IOException {

        Lexer lexicalAnalyzer = new Lexer();


        String filePath = "file_tests.txt";

        if (lexicalAnalyzer.initialize(filePath)) {

            Token token;
            try {
                while ((token = lexicalAnalyzer.nextToken()) != null) {
                    System.out.println(token);
                }
            } catch (LexerException e) {
                e.printStackTrace();
            }catch (Exception e){
                e.printStackTrace();
            }

        } else
            System.out.println("File not found!!");
    }

}
